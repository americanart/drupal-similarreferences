<?php

/**
 * @file
 * Provide views data for similarreferences.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function similarreferences_views_data_alter(&$data) {

  $data['node']['similarreferences'] = [
    'group' => t('Similar by References'),
    'title' => t('Similarity'),
    'help' => t('Percentage/count of similarity score.'),
    'field' => [
      'id' => 'similar_references_field',
    ],
    'sort' => [
      'id' => 'similar_references_sort',
    ],
  ];

  $data['node']['similar_references_nid'] = [
    'title' => t('Nid'),
    'group' => t('Similar by References'),
    'help' => t('ID of content item(s).'),
    'argument' => [
      'id' => 'similar_references_arg',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ],
  ];

}
