Similar By References
---------------------

[Similar By References](http://drupal.org/project/similarreferences) creates lists of nodes sorted by their similarity 
to a given node, based on commonality of content referenced in one or more Entity Reference fields. This is done using 
Views with custom contextual filters and sort criteria. 

### Creating a View

- **Contextual filters** - Similar to what? First off, Similar By References needs to know what node it is going to show 
stuff similar to. This is done through the "Contextual filters" (Advanced) section of the Views interface. Add the 
_"Similar by References: Nid"_ contextual filter, selecting to provide the default value with a type of "Content ID 
from URL". You can also select which Entity Reference fields to use to calculate similarity. This might be fields 
referencing other Nodes, Taxonomy Terms, Users, etc., but the selected fields must exist on the desired Content Type to 
be useful. Keep in mind that the more fields used, the better for finding similarity. However, more fields will create 
slower database queries. Be sure to cache blocks and Views output where possible.
           
- **Sort criteria** - The contextual filter alone won't effect View results. To do so, you'll need to add a sort 
criteria. Add _"Similar By References: Similarity"_ as your first sort criteria and results will be sorted by 
similarity. Set sort order to "Descending" to show most similar stuff at the top. You may also want to provide 
secondary sorting, so that nodes with the same number of common terms are sorted either alphabetically, or by date.

- **Fields** - Optionally, you may add the _"Similar By References"_ field to Fields to output the calculated 
"similarity" of each result item. This can be output either as a percentage (recommended), or as a raw count of the 
number of references the nodes have in common.

### Example 

Given an example site with two content types Recipes and Chefs, we want to display a block with "similar recipes" on 
each Recipe Node. The Recipe content type has an Content Entity Reference field which references "Chef" Nodes, and a 
Taxonomy Term Entity Reference field which references terms from a "Tags" Vocabulary. A similar recipe is one that has 
the same tags as the given recipe or is attributed to the same Chef. Create a View with a "Block" display 
as described above and configure the block visibility to display on the Recipe content type.

#### Example results

    Given Recipe (passed as argument):
      Node 1
        field_chefs: "Julia Child", "Jamie Oliver"
        field_tags: "Dinner", "Beef", "Healthy", "Slow Cooker"
        (6 references total)
    Results:
      Node 2
        field_chefs: "Jamie Oliver"
        field_tags: "Dinner", "Beef", "Healthy", "Slow Cooker", "Holidays", "Savory"
        (5 references in common with given = similarity 83%)
      Node 3
        field_chefs: "Julia Child", "Jamie Oliver"
        field_tags: "Dinner", "Beef", "Healthy", "Grill"
        (5 references in common  = similarity 83%)
      Node 4
        field_chefs: "Bobby Flay"
        field_tags: "Dinner", "Beef", "Healthy", "Slow Cooker", "American", "BBQ", "Southwest"
        (4 references in common = similarity 67%)
      Node 5
        field_chefs: "Julia Child"
        field_tags: "Dinner", "Beef", "Roast", "Cabbage Based", "Savory"
        (3 references in common = similarity 50%)
      Node 6
        field_chefs: "Julia Child", "Jamie Oliver", "Guy Fieri"
        field_tags: "Breakfast", "Eggs", "Easy"
        (2 references in common = similarity 33%)
        
### Similar projects and how they are different

The [Similar By Terms](https://www.drupal.org/project/similarterms) module adds a custom Views Argument to calculate
the "similarity" of Nodes by Taxonomy Terms instead of by any Entity Reference field. This same functionality can be 
accomplished with this module, however because Similar By Terms can use the `taxonomy_index` table it may perform better
 than this module. If you _only_ need to display similar content based on Terms, you might want to use Similar by Terms.

